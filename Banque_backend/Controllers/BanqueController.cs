using Banque_backend.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Banque_backend.Properties.Model;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Banque_backend.Persistance;

namespace Banque_backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BanqueController : ControllerBase
    {
        //public void Dispose() => Context.Dispose();
        private BanqueDbContext Context { get; }
        private readonly IConfiguration _config;
        private string mon_token;
        public BanqueController(IConfiguration config)
        {
            this.Context = new BanqueDbContext();
            _config = config;

        }

        // To generate token
        private string GenerateToken(UserLogin user)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(ClaimTypes.NameIdentifier,user.Email),
                new Claim(ClaimTypes.Role,user.Code)
            };

            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
                _config["Jwt:Audience"],
                claims,
                expires: DateTime.Now.AddMinutes(30),
                signingCredentials: credentials
            );


            return new JwtSecurityTokenHandler().WriteToken(token);

        }

        //typologie///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        [HttpGet("typologies/{parent}")]
        public List<Tuple<int,string>> GetTypo(int parent)
        {
            using (var db =Context)
            {
                var typos = db.Typologie.Where(x => x.parent == parent).Select(x => Tuple.Create(x.id, x.valeur)).ToList();
                return typos;
            }
            
        }

        
        //clients////////////////////////////////////////////////////////////////////////////////////////////////////////
        [HttpGet("exist/{email}")]
        public bool ExistClient(string email)
        {
            using (var db = Context)
            {
                var c = db.Client.Where(w => w.email == email);
                if (c.ToList().Count == 1)
                {
                    return true;
                }
            }
            return false;
        }

      
        [HttpGet("session/{email}/{code}")]
        public ActionResult GetSession(string email, string code)
        {
            using (var db = Context)
            {
                var q =db.Client.Where(w => w.email == email && w.code == code);
                if (q.Any())
                {
                    var token = GenerateToken(new UserLogin(email,code));
                    mon_token = token;
                    return Ok(token);
                }
                
                return NotFound("Email ou Mot de passe incorrecte" );
            }
        }


        
        [HttpGet("client/{email}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public Client GetClient(string email)
        {
            using (var db = Context)
            {
                var q = db.Client.Where(w => w.email == email).First<Client>();
                return q;

            }
        }

        //[HttpGet("clients")]
        //public IEnumerable<Client> GetClients()
        //{
        //    using (var db = Context)
        //    {
        //        return db.Client.OrderBy(w => w.nom).ToList();
        //    }
        //}

        [HttpPut("nouveau-client")]
        public string CreateClient(Client c)
        {
            
                using (var db = Context)
                {
                    if (!db.Client.Where(w => w.email == c.email).Any())
                    {
                        // on crée le client
                        db.Client.Add(c);
                        db.SaveChanges();
                        //on lui crée un compte
                        Compte cpt = new Compte();
                        cpt.solde = 0;
                        cpt.code = "compte_" + c.id;
                        cpt.clientid = c.id;
                        cpt.date_maj = DateTime.Now;
                        cpt.date_creation = DateTime.Now;
                        db.Compte.Add(cpt);
                        db.SaveChanges();
                        return "Votre compte client a bien été crée.";
                    }

                    else
                    {
                        return "Un compte existe deja avec cette adresse email.";
                    }
                }      
        }

        //comptes/////////////////////////////////////////////////////////////////////////////////////////////////////////
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("comptes/{clientid}")]
        public IEnumerable<Compte> GetComptes(int clientid)
        {
            using (var db = Context)
            {
                var cpts = db.Compte.Where(w => w.clientid== clientid).ToList();
                return cpts;
            }
        }

        [HttpGet("compte/{clientid}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public Compte GetCompte(int clientid)
         {
             using (var db = Context)
             {
                 var c = db.Compte.Where(x => x.clientid == clientid).First<Compte>();
                 return c;
             }
         }

        //operations/////////////////////////////////////////////////////////////////////////////////////////////////////////
        [HttpGet("operations/{clientid}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public IEnumerable<Operation> GetHistorique(int clientid)
        {
            using (var db = Context)
            {
                //on récupére les comptes du client
                var cpts_ids = db.Compte.Where(x => x.clientid == clientid).Select(x=>x.id).ToList();
                //on récupére les opérations des comptes du clientid
                var ops = db.Operation.Where(x => cpts_ids.Contains(x.compteid)).ToList().OrderByDescending(x => x.date_creation);
                
                return ops;
            }
        }

        [HttpPut("retrait/{compteid}/{montant}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public string SetRetrait( int compteid, decimal montant)
        {
            using (var db = Context)
            {
                //on récupére le compte du client
                Compte c = db.Compte.Single(x => x.id == compteid);
                if (c.solde>= montant)
                {
                    //on crée l'opération
                    Operation o = new Operation();
                    o.date_maj = DateTime.Now;
                    o.date_creation = DateTime.Now;
                    o.compteid = c.id;
                    o.montant = montant;
                    o.typologieid = db.Typologie.Where(x => x.parent == 1 && x.valeur.ToLower() == "retrait").First<Typologie>().id;
                    db.Operation.Add(o);

                    //on met a jour le solde dans la table compte      
                    c.solde = c.solde - montant;
                    db.Compte.Update(c);

                    //on save
                    db.SaveChanges();
                    c.OperationsList = db.Operation.OrderBy(w => w.id).ToList();
                    return "Votre compte a été débité d'un montant de :" + montant + "€"; 
                }
                else
                {
                    return "Votre solde actuel est insuffisant ("+c.solde+ "€)";
                }
            }
        }


        [HttpPut("depot/{compteid}/{montant}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public string SetDepot(int compteid,decimal montant)
        {
            using (var db = Context)
            {
                if (montant > 0)
                {
                    //on récupére le compte du client
                    Compte c = db.Compte.Single(x => x.id == compteid);

                    //on crée l'opération
                    Operation o = new Operation();
                    o.date_maj = DateTime.Now;
                    o.date_creation = DateTime.Now;
                    o.compteid = c.id;
                    o.montant = montant;
                    o.typologieid = db.Typologie.Where(x => x.parent == 1 && x.valeur.ToLower() == "versement").First<Typologie>().id;
                    db.Operation.Add(o);

                    //on met a jour le solde dans la table compte      
                    c.solde = c.solde + montant;
                    db.Compte.Update(c);

                    //on save
                    db.SaveChanges();
                    c.OperationsList = db.Operation.OrderBy(w => w.id).ToList();
                    return "Votre compte a été crédité d'un montant de : " + montant + "€";
                }
                else
                {
                    return "Une erreur s'est produite veuillez contactez votre conseiller, merci.";
                }
                
            }
            
        }
    }
}