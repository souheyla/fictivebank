﻿using Banque_backend.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.IO;
using Microsoft.Extensions.Configuration;


namespace Banque_backend.Persistance
{
    public class BanqueDbContext: DbContext{
        public BanqueDbContext()
        {
        }

        public DbSet<Client> Client { get; set; }
        public DbSet<Compte> Compte { get; set; }
        public DbSet<Operation> Operation { get; set; }
        public DbSet<Typologie> Typologie { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            IConfiguration _configuration = builder.Build();
            var myConnectionString = _configuration.GetConnectionString("DBConnectionString");


            base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseNpgsql(myConnectionString);
            optionsBuilder.LogTo(message => Console.WriteLine(message));
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            var dateTimeConverter = new ValueConverter<DateTime, DateTime>(
                v => v.ToUniversalTime(),
                v => DateTime.SpecifyKind(v, DateTimeKind.Utc));

            var nullableDateTimeConverter = new ValueConverter<DateTime?, DateTime?>(
                v => v.HasValue ? v.Value.ToUniversalTime() : v,
                v => v.HasValue ? DateTime.SpecifyKind(v.Value, DateTimeKind.Utc) : v);

            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                if (entityType.IsKeyless)
                {
                    continue;
                }

                foreach (var property in entityType.GetProperties())
                {
                    if (property.ClrType == typeof(DateTime))
                    {
                        property.SetValueConverter(dateTimeConverter);
                    }
                    else if (property.ClrType == typeof(DateTime?))
                    {
                        property.SetValueConverter(nullableDateTimeConverter);
                    }
                }
            }


        }
    }
}
