﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banque_backend.Model
{
    [Table("operation")]
    public class Operation
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public decimal montant { get; set; }
        public int compteid { get; set; }
        public int typologieid { get; set; }
        public DateTime date_creation { get; set; }
        public DateTime date_maj { get; set; }
    }
}
