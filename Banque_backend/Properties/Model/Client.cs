﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banque_backend.Model
{
    [Table("client")]
    public class Client
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string code { get; set; }
        public string nom { get; set; }
        public string prenom { get; set; }
        public string email { get; set; }
        public string telephone { get; set; }
        public int civilite { get; set; }
        public DateTime date_creation { get; set; }
        public DateTime date_maj { get; set; }

    }
}
