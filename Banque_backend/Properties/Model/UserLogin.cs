﻿namespace Banque_backend.Properties.Model
{
    public class UserLogin
    {
        public UserLogin(string email, string code)
        {
            this.Email = email;
            this.Code = code;

        }
        public string Email { get; set; }
        public string Code { get; set; }
    }
}
