﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Banque_backend.Model
{
    [Table("compte")]
    public class Compte
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public string code { get; set; }
        public int clientid { get; set; }
        public decimal solde { get; set; }
        public DateTime date_creation { get; set; }
        public DateTime date_maj { get; set; }
        public List<Operation> OperationsList { get; set; }
    }
}
