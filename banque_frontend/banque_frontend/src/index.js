import React from 'react';
import ReactDOM from 'react-dom/client';
import encodeUtf8 from 'encode-utf8'
import utf8 from 'utf8'
import { BrowserRouter, Routes, Route, Link, useNavigate } from "react-router-dom";
import { useState } from 'react';

import './index.css';
import Navbar from './components/Navbar';
import reportWebVitals from './reportWebVitals';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <BrowserRouter >
            <Navbar />
        </BrowserRouter>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
