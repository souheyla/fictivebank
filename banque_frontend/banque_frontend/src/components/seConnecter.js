import { createContext, useEffect, useContext, useReducer ,Component} from 'react';
import { store, useGlobalState } from 'state-pool';
import sha256 from 'sha256';
import axios from "axios";
import { Navigate, useNavigate, } from "react-router-dom";

import '../css/se_connecter.css'

import Navbar from './Navbar';

import { GetToken } from './global'


export default function SeConnecter() {
    const navigate = useNavigate();

    
    useEffect(() => {
        var minute_ms = 780000;//13mn
        const interval = setInterval(() => {
            if (window.localstorage.getitem('connected')) {
                var t = window.localStorage.getItem('SESSION-FICTIVE-BANK').replace("\"", '').replace("\"", '').split('/');
                GetToken(t[0],t[1]);
            }
                
        }, minute_ms);

        return () => clearInterval(interval); // this represents the unmount function, in which you need to clear your interval to prevent memory leaks.
    }, [])

    //fcts------------------------------------------------------------------------------------------------------------
    const SeConnecter = () => {
        document.getElementById('btn-valider').style.pointerEvents = 'none';
        document.getElementById('btn-valider').style.opacity = '0.33';

        var email = document.getElementById("email").value;
        var mdp = sha256(document.getElementById("mdp").value);

        var url = "https://localhost:7032/Banque/session/" + email + "/" + mdp;
        axios.get(url)
            .then(res => {
                var a = res.data.substring(0, 20);
                var b = res.data.substring(20);
                window.localStorage.setItem('TOKEN', JSON.stringify(a));
                document.cookie = b;

                axios.defaults.headers.common['Authorization'] = `Bearer ${res.data}`;
                url = "https://localhost:7032/Banque/client/" + document.getElementById("email").value;
                axios.get(url)
                    .then(res => {
                        var val = res.data.email + '/' + res.data.nom + ' ' + res.data.prenom+'/'+ res.data.code;
                        window.localStorage.setItem('SESSION-FICTIVE-BANK', JSON.stringify(val));
                        window.localStorage.setItem('CONNECTED', JSON.stringify(true));
                        document.getElementById('btn-valider').style.pointerEvents = 'auto';
                        document.getElementById('btn-valider').style.opacity = '1';
                        navigate('/', { replace: true });
                    })
                    .catch((error) => {
                        alert("Emeil ou mot de passe incorrect")
                    });
            })
            .catch((error) => {
                console.log(error);
            });

        
    }

   

    return (
        <div id="se_connecter">
            <div id="container">
                <div className="text-fc-2">
                    Se connecter
                </div>
                <label>1. Identifiez-vous avec votre num&#233;ro client</label>
                <input type="text" id="email" maxLength="32"/>
                <label>2. Votre code secret (6 CHIFFRES)</label>
                <input type="password" id="mdp" maxLength="6" />
                <div className="don" id="btn-valider" onClick={() => SeConnecter()}>
                    Valider
                </div>
            </div>
        </div>
    );
    


}
