import { NavLink } from 'react-router-dom'
import React from "react";
import { Routes, Route, Navigate, useNavigate } from "react-router-dom";
import axios from "axios";
import { useState, useEffect } from 'react';

import Home from './home';
import Profile from './profile';
import DevenirClient from './devenirClient';
import SeConnecter from './seConnecter';
import Routeur from './routes';

import '../css/Navbar.css'

const MyContext = React.createContext('light');

export default function Navbar() {
    
    const client = window.localStorage.getItem('SESSION-FICTIVE-BANK') != null ? window.localStorage.getItem('SESSION-FICTIVE-BANK').replace("\"", '').replace("\"", '').split('/') : '';

    const navigate = useNavigate();
    const [mounted, setMounted] = useState(false)

    const deconnexion = () => {
        window.localStorage.removeItem('SESSION-FICTIVE-BANK');
        window.localStorage.removeItem('CONNECTED');
        window.localStorage.removeItem('TOKEN');
        navigate('/', { replace: true });
    }
    return (
        <>

            <nav className="navbar" id="navbar">
                <div className="container">
                    <div className="logo" style={{ display: 'flex' }}>
                        <img id="logo_banque" src="assets/banque.png" alt="Karre" className="btn" />
                        {(window.localStorage.getItem('CONNECTED')) === "true" && (
                            <p style={{ margin: '16PX' }}>Bonjour {client[1]}</p>
                        )}
                    </div>

                    {(window.localStorage.getItem('CONNECTED')) !== "true" && (
                        <div className="nav-elements" id="nav-elements-not-connected">
                            <ul >
                                <li key={0} >
                                    <NavLink to="/" >
                                        Acceuil
                                    </NavLink>
                                </li>
                                <li key={1} >
                                    <NavLink to="/devenir-client" >
                                        <a className="button button-jaune button-md-medium text-marine ml-3 ml-1-md">Devenir client</a>
                                    </NavLink>
                                </li>
                                <li key={2} >
                                    <NavLink to="/se-connecter" >
                                        <a className="button button-jaune button-md-medium text-marine ml-3 ml-1-md">Se connecter</a>
                                    </NavLink>
                                </li>
                            </ul>

                        </div>
                    )}
                    {(window.localStorage.getItem('CONNECTED')) === "true" && (
                        <div className="nav-elements" id="nav-elements-connected" >
                            <ul >
                                <li key={0} >
                                    <NavLink to="/" >Acceuil</NavLink>
                                </li>
                                <li key={1} >
                                    <NavLink to="/profile" >Mon espace</NavLink>
                                </li>
                                <li key={2} >
                                    <p style={{ cursor: 'pointer' }} onClick={() => deconnexion()}>D&#233;connexion</p>
                                </li>
                            </ul>

                        </div>
                    )}
                </div>
            </nav>
            <div id='Routes'>
                <Routeur />
            </div>
            <div id="popup"></div>
        </>

    )

}