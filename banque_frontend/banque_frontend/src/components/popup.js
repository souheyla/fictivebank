import React from 'react';
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import ReactDOM from 'react-dom';
import '../css/Popup.css';

import x from "../assets/x.png";
import { affichePopup } from './config'

function ClosePopupn() {
    document.getElementById('popup').style.display = "none"
    document.getElementById('Routes').style.opacity = "initial";
}

export default class Popup extends React.Component {

    render() {
        return (
            <div>
                <span className="title" >
                    <h3>{this.props.titre}</h3>
                    <img id="logo_close" style={{ backgroundImage: `url(${x})`}} alt="" title="Fermer" onClick={() => ClosePopupn()} />
                </span>
                <div id="popup_content" style={{ padding: '75px' } }>
                </div>
            </div>
        );
    }
};
