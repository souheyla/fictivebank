import React, { Component } from 'react';
import sha256 from 'sha256';
import axios from "axios";
import { Navigate, useNavigate } from "react-router-dom";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

import '../css/devenir_client.css'


export default function DevenirClient() {
    const navigate = useNavigate();
    const MySwal = withReactContent(Swal);
    const CreateClient = () => {
        var bool = VerifInputValues();
        if (bool) {
            document.getElementById('btn-valider').style.pointerEvents = 'none';
            document.getElementById('btn-valider').style.opacity = '0.33';
            var url = "https://localhost:7032/Banque/exist/" + document.getElementById("email").value;
            axios.get(url)
                .then(res => {
                    if (!res.data) {
                        axios.put('https://localhost:7032/Banque/nouveau-client', {
                            code: sha256(document.getElementById("mdp").value),
                            nom: document.getElementById("nom").value,
                            prenom: document.getElementById("prenom").value,
                            email: document.getElementById("email").value,
                            telephone: document.getElementById("telephone").value,
                            civilite: parseInt(document.getElementById("civilite").value),
                            date_creation: new Date().toISOString(),
                            date_maj: new Date().toISOString()
                        }
                        )
                            .then(res => {
                                document.getElementById('btn-valider').style.pointerEvents = 'auto';
                                document.getElementById('btn-valider').style.opacity = '1';
                                navigate('/se-connecter', { replace: true });
                            })
                            .catch((error) => {
                                alert(error)
                            })
                            ;
                    }

                })
                .catch((error) => {
                    alert(error)
                })
                ;
        }
        
        
    }

    const VerifInputValues = () => {
        var bool = true;
        for (let e of document.getElementsByTagName('input')) {
            if (e.value == "") {
                MySwal.fire({
                    title: <strong>Veuillez remplir les champs obligatoires</strong>,
                    icon: 'error'
                })
                return false;
            }
        }
        return true;

    }
    return (
        <div id="devenir_client">
            <div id="container">
                <h2><u>Ouverture de votre compte individuel</u></h2>
                <div className="text-fc-2">
                    Je saisis mes informations ci-dessous
                </div>
                <select id="civilite" >
                    <option value="">Civilit&#233; *</option>
                    <option value="1">Homme</option>
                    <option value="2">Femme</option>
                    <option value="3">Non d&#233;fini</option>
                </select>
                <input type="text" id="nom" maxLength="32" title="Nom de naissance" placeholder="Nom de naissance *" />
                <input type="text" id="prenom" maxLength="32" title="Pr&#233;nom" placeholder="Pr&#233;nom *" />
                <input type="tel" id="telephone" maxLength="10" title="N&deg; de t&#233;l&#233;phone" placeholder="N&deg; de t&#233;l&#233;phone *" />
                <input type="email" id="email" maxLength="50" title="E-mail" placeholder="E-mail *" />
                <input type="email" id="email_confirmation" maxLength="50" title="Confirmation de votre E-mail" placeholder="Confirmation de votre E-mail *" />
                <input type="password" id="mdp" maxLength="6" title="Mot de passe (6 Chiffres)" placeholder="Mot de passe *" />
                <div className="don" id="btn-valider" onClick={() => CreateClient()}>
                    Valider
                </div>
            </div>
        </div>
    );
}

