import React, { useRef ,Component } from 'react';
import axios from "axios";
import $ from 'jquery';
import { createRoot } from 'react-dom/client';
import linq from "linq";
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'

import { GetToken } from './global'

import '../css/Profile.css'

const MySwal = withReactContent(Swal)


export default class Profile extends Component {

    constructor(props) {
        super(props);
        window.profile = this;
        this.state = {
            client: [],
            comptes: [],
            operations: [],
            typologies: []
        };        
    }
    componentWillMount() {
        
        const email = window.localStorage.getItem('SESSION-FICTIVE-BANK').replace("\"", '').replace("\"", '').split('/')[0];
        const token = window.localStorage.getItem('TOKEN').replace("\"", '').replace("\"", '') + document.cookie;

        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        var url = "https://localhost:7032/Banque/client/" + email;
        axios.get(url)
            .then(res => {
                this.setState({
                    client: res.data
                });
                this.updateComptes(res.data.id);
                this.jQueryCode('profile');
            });
     
        axios.get("https://localhost:7032/Banque/typologies/1")
            .then(res => {
                window.profile.setState({
                    typologies: res.data
                });
            });
    }
    componentDidUpdate() {
        var t=document.getElementById('profile_content').getElementsByTagName('input');
        for (var i = 0; i < t.length; i++) {
            switch (t[i].id) {
                case 'nom':
                    t[i].value = this.state.client.nom;
                    break;
                case 'prenom':
                    t[i].value = this.state.client.prenom;
                    break;
                case 'email':
                    t[i].value = this.state.client.email;
                    break;
                case 'telephone':
                    t[i].value = this.state.client.telephone;
                    break;
            }
        }
       
    }

    jQueryCode = (id) => {
        if (id == "profile") {
            window.profile.displayDiv(id);
        }
        $(function () {
            $(".nav-item").click(function (e) {
                e.preventDefault();
                $(".nav-item" ).removeClass("active");
                $(this).addClass("active");
                id = $(this)[0].id;
                if (id == 'historique') {
                    axios.get("https://localhost:7032/Banque/operations/" + window.profile.state.client.id)
                        .then(res => {
                            if (res.data.length > 0) {
                                window.profile.setState({
                                    operations: res.data
                                });  
                            }
                        }); 
                    
                }
                setTimeout(() => {
                    window.profile.displayDiv(id);
                }, 200);
            });

        });


    }

    displayDiv(id) {
        for (let element of document.getElementsByClassName("operations")) {
            element.style.display = "none";
        }
        if (document.getElementById(id + "_content"))
            document.getElementById(id + "_content").style.display = "block";
    }

    updateComptes(clientid) {
        axios.get("https://localhost:7032/Banque/comptes/" + clientid)
            .then(res => {
                this.setState({
                    comptes: res.data
                });
            });    
    }

    depotRetrait(type) {
        if (type == 'd') {
            axios.put('https://localhost:7032/Banque/depot/' + document.getElementById("cpt_depot").value + '/' + document.getElementById("montant_depot").value)
                .then(res => {
                    this.updateComptes(this.state.client.id);
                    MySwal.fire({
                        title: <strong>{ res.data}</strong>,
                        icon: 'success'
                    })
                    document.getElementById("montant_depot").value = "";
                })
                .catch((error) => {
                    console.log(error)
                });
        }
        else {
            axios.put('https://localhost:7032/Banque/retrait/' + document.getElementById("cpt_retrait").value + '/' + document.getElementById("montant_retrait").value)
                .then(res => {
                    this.updateComptes(this.state.client.id);
                    MySwal.fire({
                        title: <strong>{res.data}</strong>,
                        icon: 'success'
                    })
                    document.getElementById("montant_retrait").value = "";
                })
                .catch((error) => {
                    console.log(error)
                });
        }


    }

    
    handleMoneyChange(e,id) {
        if (e.key !== "Backspace") {
            if (document.getElementById(id).value.includes('.')) {
                if (document.getElementById(id).value.split('.')[1].length >= 2) {
                    var num = parseFloat(document.getElementById(id).value);
                var cleanNum = num.toFixed(2);
                    document.getElementById(id).value = cleanNum;
                e.preventDefault();
            }
        }
    }
}
    

    render() {
        return (
            <div id="mon_profile">
                <div id="container" className="container">
                <div style={{ display: "flex", height: '100 %' }}>
                    <div style={{ width: "35%" }}>
                        <nav className="side-nav">
                            <ul className="nav-menu">
                                <li className="nav-item active" id="profile" >
                                    <a href="#"  >
                                        <i className="fas fa-tachometer-alt"></i>
                                        <span className="menu-text">Mes informations de profile</span>
                                    </a>
                                </li>
                                <li className="nav-item" id="solde" name="solde">
                                    <a href="#">
                                        <i className="fas fa-tachometer-alt"></i>
                                        <span className="menu-text">Op&#233;rations  &#x21DD; Consuler mon solde</span>
                                    </a>
                                </li>

                                <li className="nav-item" id="retrait" name="retrait">
                                    <a href="#">
                                        <i className="fas fa-tachometer-alt"></i>
                                        <span className="menu-text">Op&#233;rations  &#x21DD; Effectuer un retrait</span>
                                    </a>
                                </li>
                                <li className="nav-item" id="depot" name="depot">
                                    <a href="#">
                                        <i className="fas fa-tachometer-alt"></i>
                                        <span className="menu-text">Op&#233;rations  &#x21DD; Effectuer un d&#233;pot</span>
                                    </a>
                                </li>
                               

                                <li className="nav-item" id="historique" name="historique">
                                    <a href="#">
                                        <i className="fas fa-tachometer-alt"></i>
                                        <span className="menu-text">Op&#233;rations   &#x21DD; Mon historique d'op&#233;rations</span>
                                    </a>
                                </li>

                            </ul>
                        </nav>
                    </div>

                    <div id="container_content" style={{ width: "60%" }}>
                        <div id='profile_content' className="operations" >
                            <h2><u>Mes informations personelles</u></h2>
                            <div className="display">
                                <label>Nom de naissance</label>
                                <input  type="text" id="nom" disabled={true} maxLength="32" title="Nom de naissance" placeholder="Nom de naissance" />
                            </div>
                            <div className="display">
                                <label>Pr&#233;nom</label>
                                <input type="text" id="prenom" disabled={true} maxLength="32" title="Pr&#233;nom" placeholder="Pr&#233;nom" />
                            </div>
                            <div className="display">
                                <label>N&deg; de t&#233;l&#233;phone</label>
                                <input  type="tel" id="telephone" disabled={true} maxLength="10" title="N&deg; de t&#233;l&#233;phone" placeholder="N&deg; de t&#233;l&#233;phone" />
                            </div>
                            <div className="display">
                                <label>E-mail</label>
                                <input  type="email" id="email" disabled={true} title="E-mail" placeholder="E-mail" />
                            </div>
                        </div>

                        <div className="operations" id="retrait_content" style={{ display: "none" }}>
                            <h2><u>Effectuer un retrait:</u></h2>
                            <div className="display">
                                <label>N&deg; compte:</label>
                                <select name="pets" id="cpt_retrait">
                                    {
                                        this.state.comptes.map((c, key) => {
                                            return (
                                                <option key={key} value={c.id}>{c.code}</option>
                                            );
                                        })}
                                </select>
                            </div>

                            <div className="display">
                                <label>Montant :</label>
                                    <input type="number" onKeyDown={(e) => { this.handleMoneyChange(e,"montant_retrait")}}  id="montant_retrait" title="Montant" placeholder="Montant" />
                            </div>

                            <div className="don" id="btn-valider" onClick={() => this.depotRetrait('r')}>
                                Valider
                            </div>
                        </div>

                        <div className="operations" id="depot_content" style={{ display: "none" }}>
                            <h2><u>Effectuer un versement:</u></h2>
                            <div className="display">
                                <label>N compte:</label>
                                <select name="pets" id="cpt_depot">
                                    {

                                        this.state.comptes.map((c, key) => {
                                            return (
                                                <option key={key}  value={c.id }>{c.code }</option>
                                            );
                                        })}
                                </select>
                            </div>

                            <div className="display">
                                <label>Montant :</label>
                                    <input type="number" id="montant_depot" onKeyDown={(e) => { this.handleMoneyChange(e, "montant_depot") }}  title="Montant" placeholder="Montant" />
                            </div>

                            <div className="don" id="btn-valider" onClick={() => this.depotRetrait('d')}>
                                Valider
                            </div>

                        </div>

                        <div className="operations" id="solde_content" style={{ display: "none" }}>
                            <table>
                                <thead>
                                    <tr>
                                        <th colSpan="2">Liste de vos comptes</th>
                                    </tr>
                                    <tr>
                                        <th >N&deg; de compte</th>
                                        <th >Solde</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {

                                        this.state.comptes.map((o, key) => {
                                            return (
                                                <tr key={key}>
                                                    <td>{o.code}</td>
                                                    <td>{o.solde.toFixed(2)} &euro;</td>
                                                </tr>
                                            );
                                        })
                                    }
                                    
                                </tbody>
                                <tfoot>
                                    <tr >
                                        <th >Total</th>
                                        <th >{linq.from(this.state.comptes).select(x => x.solde).sum()} &euro;</th>
                                    </tr>
                                </tfoot>
                            </table>


                        </div>

                        <div className="operations" id="historique_content" style={{ display: "none" }}>

                            <table>
                                <thead>
                                    <tr>
                                        <th colSpan="4">Liste de vos operations</th>
                                        </tr>
                                        <tr>
                                            <th >Date op</th>
                                            <th >Type op</th>
                                            <th >Montant en &euro;</th>
                                        </tr>
                                </thead>
                                <tbody>
                                    {

                                        this.state.operations.map((o, key) => {
                                            return (
                                                
                                                <tr key={key}>
                                                    <td>{o.date_creation.split('T')[0]  }</td>
                                                    <td>{linq.from(this.state.typologies).where(x => x.item1 == o.typologieid).select(x => x.item2)}</td>
                                                    <td>{o.montant.toFixed(2)} &euro;</td>
                                                </tr>
                                            );
                                        })
                                    }

                                </tbody>
                            </table>


                        </div>




                    </div>

                </div>


            </div>
            </div>
        );
    }

}
