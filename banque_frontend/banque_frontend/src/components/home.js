import React, { Component } from 'react';

import background from "../assets/home.PNG";

export default class Home extends Component {
    static displayName = Home.name;

    constructor(props) {
        super(props);
        this.state = { forecasts: [], loading: true };
    }

    componentDidMount() {
        this.populateWeatherData();
    }

   
    render() {
        
        return (
            <div>
                <div className="basic-html aem-GridColumn aem-GridColumn--default--12">
                    <div className="emergency-banner emergency-banner--default-theme">
                        <div className="emergency-banner_text">
                            <p className="emergency_title-bann"><b>Seisme en Turquie/Syrie : mobilisons-nous !</b></p>
                            <p>Face a la situation, vous pouvez exprimer votre solidarite en faisant un don au Fonds Urgence et Developpement. Son montant sera double par FICTIVEBANK pour financer concretement les actions de nos ONG partenaires sur le terrain (la Croix-Rouge, CARE et Medecins Sans Frontieres).</p>
                        </div>
                        <div className="emergency-banner_btn">
                            <a className="don" target="blank" href="https://dons-urgenceetdeveloppement.bnpparibas.com/site/bnp/2023_seisme_turquie/fr/don/index.html?donorType=C&amp;from=2023STC">Je fais un don</a>
                        </div>
                    </div>

                    <div id="home" style={{ backgroundImage: `url(${background})`, height: '838px', backgroundPositionX:'center' }} />

                </div>
                
            </div>
        );
    }

    async populateWeatherData() {
        const response = await fetch('weatherforecast');
        const data = await response.json();
        this.setState({ forecasts: data, loading: false });
    }
}
