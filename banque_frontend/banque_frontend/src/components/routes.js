import React from "react";
import { Redirect, Switch, Route, Router, Routes, Navigate, useNavigate } from "react-router-dom";

import Home from './home';
import Profile from './profile';
import DevenirClient from './devenirClient';
import SeConnecter from './seConnecter';


function Routeur() {
    return (
        <Routes >
            <Route exact path='/' element={<Home />} />
            <Route exact path='/profile' element={<Profile />} />
            <Route exact path='/devenir-client' element={<DevenirClient />} />
            <Route exact path='/se-connecter' element={<SeConnecter />} />
            <Route exact path='/deconnexion' element={<Home />} />
        </Routes>
        
    );
}

export default Routeur