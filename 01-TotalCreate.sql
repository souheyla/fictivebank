CREATE TABLE typologie
(
  ID serial,
  parent int,
  valeur text,
  DATE_CREATION TIMESTAMP WITH TIME ZONE,
  DATE_MAJ TIMESTAMP WITH TIME ZONE,
  CONSTRAINT PK_typologie PRIMARY KEY (ID)
);
insert into typologie(id,valeur,DATE_CREATION,DATE_MAJ) values(1,'type operation',now(),now());
insert into typologie(id,parent,valeur,DATE_CREATION,DATE_MAJ) values(2,1,'retrait',now(),now());
insert into typologie(id,parent,valeur,DATE_CREATION,DATE_MAJ) values(3,1,'versement',now(),now());  

CREATE TABLE CLIENT
(
  ID serial,
  CODE text,
  nom text,
  DATE_CREATION TIMESTAMP WITH TIME ZONE,
  DATE_MAJ TIMESTAMP WITH TIME ZONE,
  CONSTRAINT PK_CLIENT PRIMARY KEY (ID)
);

INSERT INTO client(code, nom, date_creation, date_maj)
VALUES ('client_1', 'client_1', now(), now());


CREATE TABLE compte
(
  ID SERIAL,
  clientid int,
  CODE text,
  solde decimal,
  DATE_CREATION TIMESTAMP WITH TIME ZONE,
  DATE_MAJ TIMESTAMP WITH TIME ZONE,
  CONSTRAINT PK_compte PRIMARY KEY (ID)
);

INSERT INTO compte(clientid,code, solde, date_creation, date_maj)
VALUES (1,'compte_1', 2000,now(), now());

CREATE TABLE operation
(
  ID SERIAL ,
  montant decimal,
  compteid int,
  TYPOLOGIEID int,
  DATE_CREATION TIMESTAMP WITH TIME ZONE,
  DATE_MAJ TIMESTAMP WITH TIME ZONE,
  CONSTRAINT PK_operation PRIMARY KEY(ID)
);

