
using BankTest.Hexagoone;
using BankTest.Model;
using Microsoft.Extensions.Configuration;
using NFluent;
using System.Security.Cryptography;
using System.Text;

namespace BankTest
{
    [TestClass]
    public class UnitTest1
    {
        MockBankRepo b = new MockBankRepo();

        //typologie********************************************************************************
        [TestMethod]
        public void VerifTypoExist()
        {
            var q = b.GetTypo(1);
            Assert.IsTrue(q.Count == 2);

            q = b.GetTypo(58);
            Assert.AreEqual(0, q.Count);
        }

        //client********************************************************************************
        [TestMethod]
        public void VerifExistClient()
        {
            var q = b.ExistClient("didisouhila@gmail.com");
            Assert.IsTrue(q);

            var q2 = b.ExistClient("didisouhila2hhkk@gmail.com");
            Assert.IsFalse(q2);
        }


        [TestMethod]
        public void VerifGetClient()
        {
            var q = b.GetClient("didisouhila@gmail.com");
            Assert.IsInstanceOfType(q, typeof(Client));
            Assert.AreEqual("didi".ToLower(), q.nom.ToLower(), "***" + q.telephone);

            //var q2 = b.GetClient("didisouhilaa@gmail.com");
            //Assert.IsNull(q2.code);
        }

        [TestMethod]
        public void VerifCreateClient()
        {
            SHA256 mySHA256 = SHA256.Create();

            Client c = new Client();
            c.nom = "INAL";
            c.prenom = "TAHA";
            c.email = "taha@gmail.com";
            c.telephone = "0662870055";
            string hash = String.Empty;
            byte[] hashValue = mySHA256.ComputeHash(Encoding.UTF8.GetBytes("000000"));
            foreach (byte b in hashValue)
            {
                hash += $"{b:X2}";
            }
            c.code = hash;
            c.civilite = 1;
            c.date_creation = DateTime.Now;
            c.date_maj = DateTime.Now;

            var q = b.CreateClient(c);

            Assert.IsInstanceOfType(q, typeof(string));
            Assert.AreEqual("Votre compte client a bien �t� cr�e.", "Votre compte client a bien �t� cr�e.", "***" + q);


            c = new Client();
            c.nom = "INAL";
            c.prenom = "TAHA";
            c.email = "taha@gmail.com";
            c.telephone = "0662870055";

            foreach (byte b in hashValue)
            {
                hash += $"{b:X2}";
            }
            c.code = hash;
            c.civilite = 1;
            c.date_creation = DateTime.Now;
            c.date_maj = DateTime.Now;

            var q2 = b.CreateClient(c);

            Assert.AreNotEqual("Un compte existe deja avec cette adresse email.", q2, "***" + q);
        }

        //comptes*******************************************************************************
        [TestMethod]
        public void VerifGetComptes()
        {
            var q = b.GetComptes(1);
            Assert.AreEqual(q.Count().ToString(), "1", "***" + q.Count());

            q = b.GetComptes(0);
            Assert.IsTrue(q.Count() == 0);

        }

        //operations////////////////////////////////////////////////////////////////////////////////////

        [TestMethod]
        public void VerifGetHistorique()
        {
            var q = b.GetHistorique(1);
            Assert.IsNotNull(q);
            Assert.IsInstanceOfType(q, typeof(IEnumerable<Operation>));
            Assert.IsTrue(q.Count() != 0);

            q = b.GetHistorique(5);
            Assert.IsTrue(q.Count() == 0);
        }


        [TestMethod]
        public void VerifSetRetrait()
        {
            var q = b.SetRetrait(1, 5);
            Assert.AreEqual(q, "Votre compte a �t� d�bit� d'un montant de :" + 5 + "�");

            q = b.SetRetrait(1, 500);
            Assert.IsFalse(q == "Votre compte a �t� d�bit� d'un montant de :" + 500 + "�");
        }

        [TestMethod]
        public void VerifSetDepot()
        {
            var q = b.SetDepot(1, 5000);
            Assert.AreEqual(q, "Votre compte a �t� cr�dit� d'un montant de : " + 5000 + "�");

            q = b.SetDepot(1, 0);
            Assert.IsTrue(q == "Une erreur s'est produite veuillez contactez votre conseiller, merci.");

            q = b.SetDepot(0, 0);
            Assert.IsFalse(q == "Votre compte a �t� cr�dit� d'un montant de : " + 0 + "�");
        }


    }
}