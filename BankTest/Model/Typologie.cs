﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BankTest.Model
{
    [Table("typologie")]
    public class Typologie
    {
        [Key]
        [Column("id")]
        public int id { get; set; }
        public int parent { get; set; }
        public string valeur { get; set; }
        public DateTime date_creation { get; set; }
        public DateTime date_maj { get; set; }
    }
}
