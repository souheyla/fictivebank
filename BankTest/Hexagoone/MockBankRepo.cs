﻿using BankTest.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace BankTest.Hexagoone
{
    public class MockBankRepo:IBankRepo, IDisposable { 
  
        List<Client> clients = new List<Client>();
        List<Operation> ops = new List<Operation>();
        List<Compte> comptes = new List<Compte>();
        List<Typologie> typos = new List<Typologie>();

        public MockBankRepo()
        {
            SHA256 mySHA256 = SHA256.Create();

            Client c = new Client();
            c.id = 1;
            c.nom = "didi";
            c.prenom = "souheyla";
            c.email = "didisouhila@gmail.com";
            c.telephone = "0888888888";
            string hash = string.Empty;
            byte[] hashValue = mySHA256.ComputeHash(Encoding.UTF8.GetBytes("000000"));
            foreach (byte b in hashValue)
            {
                hash += $"{b:X2}";
            }
            c.code = hash;
            c.civilite = 1;
            c.date_creation = DateTime.Now;
            c.date_maj = DateTime.Now;

            clients.Add(c);

            Compte cpt = new Compte();
            cpt.id = 1;
            cpt.solde = 50;
            cpt.code = "compte_1";
            cpt.clientid = 1;
            cpt.date_maj = DateTime.Now;
            cpt.date_creation = DateTime.Now;

            comptes.Add(cpt);

            Operation o = new Operation();
            o.date_maj = DateTime.Now;
            o.date_creation = DateTime.Now;
            o.compteid = c.id;
            o.montant = 500;
            o.typologieid = 3;

            ops.Add(o);

            Operation o2 = new Operation();
            o.date_maj = DateTime.Now;
            o.date_creation = DateTime.Now;
            o.compteid = c.id;
            o.montant = 5;
            o.typologieid = 2;

            ops.Add(o2);


            Typologie t = new Typologie();
            t.id = 1;
            t.valeur = "type operation";
            t.date_creation = DateTime.Now;
            t.date_maj = DateTime.Now;

            Typologie t2 = new Typologie();
            t2.id = 2;
            t2.parent = 1;
            t2.valeur = "retrait";
            t2.date_creation = DateTime.Now;
            t2.date_maj = DateTime.Now;

            Typologie t3 = new Typologie();
            t3.id = 3;
            t3.parent = 1;
            t3.valeur = "versement";
            t3.date_creation = DateTime.Now;
            t3.date_maj = DateTime.Now;

            typos.Add(t);
            typos.Add(t2);
            typos.Add(t3);

        }

        public List<Tuple<int, string>> GetTypo(int parent)
        {
            var q =(
                    from item1 in typos
                    where item1.parent==parent 
                    select Tuple.Create(item1.id, item1.valeur)
                    ).ToList();
            return q;

        }

        public Client GetClient(string email)
        {
            return clients.Where(x => x.email == email).Single();
        }

        public string CreateClient(Client c)
        {
            if (!clients.Where(w => w.email == c.email).Any())
            {
                Client cli = new Client();
                cli.nom = c.nom;
                cli.prenom = c.prenom;
                cli.email = c.email;
                cli.telephone = c.telephone;

                SHA256 mySHA256 = SHA256.Create();
                string hash = string.Empty;
                byte[] hashValue = mySHA256.ComputeHash(Encoding.UTF8.GetBytes(c.code));
                foreach (byte b in hashValue)
                {
                    hash += $"{b:X2}";
                }
                c.code = hash;
                c.civilite = c.civilite;
                c.date_creation = DateTime.Now;
                c.date_maj = DateTime.Now;
                return "Votre compte client a bien été crée.";
            }
            return "Un compte existe deja avec cette adresse email.";

        }

        public IEnumerable<Compte> GetComptes(int clientid)
        {
            return comptes.Where(x => x.clientid == clientid);
        }

        public Compte GetCompte(int clientid)
        {
            return comptes.Where(x => x.clientid == clientid).Single();
        }

        public IEnumerable<Operation> GetHistorique(int clientid)
        {
            var cpts = comptes.Where(x => x.clientid == clientid).Select(x => x.id).ToList();
            return ops.Where(x => cpts.Contains(x.compteid)).ToList().OrderByDescending(x => x.date_creation);
        }

        public string SetRetrait(int compteid, decimal montant)
        {
            var q = comptes.Where(x => x.id == compteid).Single();
            if (q.solde >= montant && compteid != 0)
            {
                Operation o = new Operation();
                o.date_maj = DateTime.Now;
                o.date_creation = DateTime.Now;
                o.compteid = compteid;
                o.montant = montant;
                o.typologieid = 3;
                ops.Add(o);
                return "Votre compte a été débité d'un montant de :" + montant + "€";
            }
            return "Votre solde actuel est insuffisant (" + q.solde + "€)";
        }

        public string SetDepot(int compteid, decimal montant)
        {
            
            if (montant >0 && compteid != 0)
            {
                    
                Operation o = new Operation();
                o.date_maj = DateTime.Now;
                o.date_creation = DateTime.Now;
                o.compteid = compteid;
                o.montant = montant;
                o.typologieid = 3;
                ops.Add(o);
                return "Votre compte a été crédité d'un montant de : " + montant + "€";
            }
            return "Une erreur s'est produite veuillez contactez votre conseiller, merci.";
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }

        public bool ExistClient(string email)
        {
            var c = clients.Where(w => w.email == email);
            if (c.ToList().Count == 1)
            {
                return true;
            }
            return false;
        }
    }
}
