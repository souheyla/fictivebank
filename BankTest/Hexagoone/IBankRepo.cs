﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BankTest.Model;

namespace BankTest.Hexagoone
{
    internal interface IBankRepo
    {
        public List<Tuple<int, string>> GetTypo(int parent);
        Client GetClient(string email);
        string CreateClient(Client c);
        IEnumerable<Compte> GetComptes(int clientid);
        Compte GetCompte(int clientid);
        IEnumerable<Operation> GetHistorique(int clientid);
        string SetRetrait(int compteid, decimal montant);
        string SetDepot(int compteid, decimal montant);
        bool ExistClient(string email);
    }
}
