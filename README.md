# BANK



## Getting started

1-Dans le dossier il y’a un backup de la base de données ainsi qu’un script pour créer les tables au besoin

2-Mdifier la connectionString de votre bdd dans Banque_backend\appsettings.json

3-Avoir préalablement installé nodejs pour executer le front

## Name
FictiveBank

## Description
Cette application simule une banque, nous pouvons effectuer les opérations suivante: depot/retrait/consulter le solde/créer compte/consulter l'historique des transactions

## Visuals
1-page Home

![archi-hexa](./assets/home.jpg)

2-page de création de compte client

![archi-hexa](./assets/devenir_client.jpg)

3-page de connexion

![archi-hexa](./assets/se_connecter.jpg)

4-profile

![archi-hexa](./assets/profile.png)

5-page mon solde

![archi-hexa](./assets/solde.jpg) 

6-page retrait/dépôt

![archi-hexa](./assets/retrait_depot.jpg) 

7-page historique transactions

![archi-hexa](./assets/historique.jpg) 

